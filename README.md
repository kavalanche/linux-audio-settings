# My linux audio settings for best audio performance

## Tell ALSA to use Pulseaudio
~/.asoundrc

## Setting for Pulseaudio to make sound as good as it gets
~/.config/pulse/daemon.conf
